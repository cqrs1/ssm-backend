<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <packaging>pom</packaging>
    <modules>
        <module>config-service</module>
        <module>product-service</module>
        <module>api-gateway</module>
        <module>discovery-service</module>
    </modules>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.6.1</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <groupId>com.ssm.microservices.app</groupId>
    <artifactId>ssm-backend</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <main.basedir>${project.basedir}</main.basedir>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <spring-boot.version>2.6.1</spring-boot.version>
        <spring-cloud.version>2021.0.0</spring-cloud.version>
        <spring-cloud-starter-config.version>3.1.0</spring-cloud-starter-config.version>
        <spring-cloud-starter-netflix-eureka-server.version>3.1.0</spring-cloud-starter-netflix-eureka-server.version>
        <spring-cloud-starter-netflix-eureka-client.version>3.0.4</spring-cloud-starter-netflix-eureka-client.version>
        <spring-boot-actuator.version>2.6.1</spring-boot-actuator.version>
        <spring-boot-starter-web.version>2.6.1</spring-boot-starter-web.version>
        <spring-cloud-starter-gateway.version>3.1.0</spring-cloud-starter-gateway.version>
        <spring-cloud-starter-eureka.version>1.4.7.RELEASE</spring-cloud-starter-eureka.version>
        <spring-boot-starter-webflux.version>2.6.1</spring-boot-starter-webflux.version>
        <spring-cloud-starter-bus-amqp.version>3.0.3</spring-cloud-starter-bus-amqp.version>
        <spring-boot-starter-aop.version>2.6.1</spring-boot-starter-aop.version>
        <spring-retry.version>1.3.1</spring-retry.version>
        <spring-boot-starter-actuator.version>2.6.1</spring-boot-starter-actuator.version>
        <spring-cloud-config-monitor.version>3.0.5</spring-cloud-config-monitor.version>
        <spring-cloud-starter-stream-rabbit.version>3.1.5</spring-cloud-starter-stream-rabbit.version>
        <commons-validator.version>1.3.1</commons-validator.version>
        <reactor-test.version>3.4.13</reactor-test.version>
        <lombok.version>1.18.22</lombok.version>
        <axon-spring-boot-starter.version>4.5.7</axon-spring-boot-starter.version>
        <guava.version>31.0.1-jre</guava.version>
        <postgresql.version>42.3.1</postgresql.version>
        <flyway-core.version>8.2.3</flyway-core.version>
        <org.testcontainers.version>1.16.0</org.testcontainers.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>commons-validator</groupId>
            <artifactId>commons-validator</artifactId>
            <version>${commons-validator.version}</version>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.axonframework</groupId>
                <artifactId>axon-bom</artifactId>
                <version>${axon-spring-boot-starter.version}</version>
            </dependency>

            <dependency>
                <groupId>org.testcontainers</groupId>
                <artifactId>testcontainers-bom</artifactId>
                <version>${org.testcontainers.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-config</artifactId>
                <version>${spring-cloud-starter-config.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-eureka</artifactId>
                <version>${spring-cloud-starter-eureka.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
                <version>${spring-cloud-starter-netflix-eureka-server.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
                <version>${spring-cloud-starter-netflix-eureka-client.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-gateway</artifactId>
                <version>${spring-cloud-starter-gateway.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-bus-amqp</artifactId>
                <version>${spring-cloud-starter-bus-amqp.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-config-monitor</artifactId>
                <version>${spring-cloud-config-monitor.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
                <version>${spring-cloud-starter-stream-rabbit.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-webflux</artifactId>
                <version>${spring-boot-starter-webflux.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-actuator</artifactId>
                <version>${spring-boot-actuator.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-actuator</artifactId>
                <version>${spring-boot-starter-actuator.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web</artifactId>
                <version>${spring-boot-starter-web.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-data-jpa</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-aop</artifactId>
                <version>${spring-boot-starter-aop.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.retry</groupId>
                <artifactId>spring-retry</artifactId>
                <version>${spring-retry.version}</version>
            </dependency>

            <dependency>
                <groupId>io.projectreactor</groupId>
                <artifactId>reactor-test</artifactId>
                <version>${reactor-test.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
                <optional>true</optional>
            </dependency>

            <dependency>
                <groupId>org.axonframework</groupId>
                <artifactId>axon-spring-boot-starter</artifactId>
                <version>${axon-spring-boot-starter.version}</version>
            </dependency>

            <dependency>
                <groupId>org.axonframework</groupId>
                <artifactId>axon-test</artifactId>
                <version>${axon-spring-boot-starter.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>${postgresql.version}</version>
                <scope>runtime</scope>
            </dependency>

            <dependency>
                <groupId>org.flywaydb</groupId>
                <artifactId>flyway-core</artifactId>
                <version>${flyway-core.version}</version>
            </dependency>

            <dependency>
                <groupId>org.testcontainers</groupId>
                <artifactId>testcontainers</artifactId>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.testcontainers</groupId>
                <artifactId>junit-jupiter</artifactId>
                <version>${org.testcontainers.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.testcontainers</groupId>
                <artifactId>postgresql</artifactId>
                <version>${org.testcontainers.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.testcontainers</groupId>
                <artifactId>testcontainers</artifactId>
                <version>${org.testcontainers.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement></pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                        </exclude>
                    </excludes>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>