## CQRS && Event Sourcing iter

### Command responsibility part
1. Create CommandController and model(s) related.
2. Validate request body in command controller.
    - Validation in the @CommandHandler method(s)
    - Validation in the Message Dispatch Interceptor.
        * Create the Command Interceptor class(es) required and register them with Configuration classes.
3. Create the Command class(es).
4. Create the aggregate(s).
5. Create the event(s) linked to each command.
6. Apply and publish the event(s) in the aggregate(s).
7. Create the JPA entities, repositories.
8. Create the event(s) handler(s).
9. Implement @EventHandler method(s).

### Query responsibility part
1. Create QueryController and model(s) related.
2. Create the QueryHandler(s).

### Set Based Consistency
1. Create the lookup tables needed.
2. Create the JPA entities and repositories for them.
3. Create the EventHandler(s) for the lookup tables created.
4. Update the Message Dispatch Interceptor.

## Lombok @Builder and inheritance
1. Create a field-based constructor. This includes also the fields from the superclasses. We annotate it with @Builder, instead of the class:
Ex.:
```
@Getter
@AllArgsConstructor
public class Parent {
    private final String parentName;
    private final int parentAge;
}

@Getter
public class Child extends Parent {
    private final String childName;
    private final int childAge;

    @Builder
    public Child(String parentName, int parentAge, String childName, int childAge) {
        super(parentName, parentAge);
        this.childName = childName;
        this.childAge = childAge;
    }
}
```

2. This way, we'll be able to access a convenient builder from the Child class, which will allow us to specify also the Parent class' fields:
```
Child child = Child.builder()
  .parentName("Andrea")
  .parentAge(38)
  .childName("Emma")
  .childAge(6)
  .build();
```

## Download and run Axon server as JAR application

1. https://axoniq.io/
    - 'Download' button and register
    - 'Download now' button and download the ZIP file
2. In the terminal go to the AxonServer directory and run the command:
    java -jar axonserver-xxx.jar
3. The Axon Server dashboard is accessible at localhost:8024

## Axon server configuration properties (docs.axoniq.io)
Unless explicitly specified, all properties names are to be prefixed with **'axoniq.axonserver'**.

1. Create a new configuration file in the Axon server folder or in a subfolder.
    **config/axonserver.yml**
    Ex.:
   ```
    server:
        port: 8024

    axoniq:
        axonserver:
            name: Ssm Axon Server
            hostname: localhost
            devmode:
                enabled: true
    ```
## Run Axon Server in a Docker container (RECOMMENDED)
```
docker run --name axonserver -p 8024:8024 -p 8124:8124 -v c:/Courses/cqrs_event_driven/docker-data/data:/data -v c:/Courses/cqrs_event_driven/docker-data/eventdata:/eventdata -v c:/Courses/cqrs_event_driven/docker-data/config:/config axoniq/axonserver
```
The first port keypair is for HTTP communication and the second one is for GRPC communication.

### Using Docker compose
```
version: '3.3'
services:
  axonserver:
    image: axoniq/axonserver
    hostname: axonserver
    volumes:
      - c:/Courses/cqrs_event_driven/docker-data/axonserver-data:/data
      - c:/Courses/cqrs_event_driven/docker-data/axonserver-events:/eventdata
      - c:/Courses/cqrs_event_driven/docker-data/axonserver-config:/config:ro
    ports:
      - '8024:8024'
      - '8124:8124'
      - '8224:8224'

volumes:
  axonserver-data:
    driver: local
    driver_opts:
      type: none
      device: ${PWD}/data
      o: bind
  axonserver-events:
    driver: local
    driver_opts:
      type: none
      device: ${PWD}/events
      o: bind
  axonserver-config:
    driver: local
    driver_opts:
      type: none
      device: ${PWD}/config
      o: bind
```

## DDD layers & CQRS
A complete application consists of three major layers:

* application (CQRS commands, CQRS events, CQRS queries, CQRS handlers, controllers):
    - Through the application layer, **the user or any other program interacts** with the application. 
      This area should contain things like user interfaces, RESTful controllers, and JSON serialization libraries. 
      It includes anything **that exposes entry to our application and orchestrates the execution of domain logic**.

* domain (Aggregates, Entities, POCO domain classes, Value Objects, repositories interfaces):
    - **In the domain layer, we keep the code that touches and implements business logic**. 
      This is the core of our application. Additionally, this layer should be isolated from both the application 
      part and the infrastructure part. 
      On top of that, it should also contain interfaces that define the API to communicate with external parts, 
      like the database, which the domain interacts with.

* infrastructure (Repositories implementations):
    - **The infrastructure layer is the part that contains anything that the application needs to work** such as 
      database configuration or Spring configuration. Besides, it also implements infrastructure-dependent interfaces 
      from the domain layer.
      
The domain layer contains the domain entities and stand-alone domain services. Any domain concepts (this includes domain services, but also repositories) that depend on external resources, are defined by interfaces.

The infrastructure layer contains the implementation of the interfaces from the domain layer. These implementations may introduce new non-domain dependencies that have to be provided the application. These are the application services and are represented by interfaces.

The application layer contains the implementation of the application services. The application layer may also contain additional implementations of domain interfaces, if the implementations provided by the infrastructure layer are not sufficient.

### Aggregates
They are basically the domain objects. They will handle commands, validate them and will decide if some entity can be created.
Aggregate classes is at the core of our microservices. 
* They hold the current state of the main object.
* Additionally, the aggregates will contain methods that **can handle commands**.
* They can contain **business logic** for validation, decision making (ex.: create or not a new product)
* They can also contain **event sourcing handler methods** and everytime when the state of the domain model changes, **an event sourcing method will be invoked**.

### Services
* **Domain Services** : Encapsulates business logic that doesn't naturally fit within a domain object, and are NOT typical CRUD operations – those would belong to a Repository.
                        It provides solutions for business invariants that are too complex to be stored inside a single Entity or Value Object.
                        Domain Services does not handle sessions or requests. 
                        It does not know anything about UI components. 
                        It does not execute database migration. 
                        It does not validate user input. 
                        Domain Service only manages business logic.
                        Domain Service must NOT hold a state. Domain Service must NOT contain any field that has a state, as well.
* **Application Services** : Used by external consumers to talk to your system (think Web Services). If consumers need access to CRUD operations, they would be exposed here.
* **Infrastructure Services** : Used to abstract technical concerns (e.g. MSMQ, email provider, etc).

### Command and CommandHandler principles --> <Verb><Noun>Command
* Is a business intention, something you want a system to do. 
* Any alteration on the state of the application MUST be performed through a **Command**.
* Keep the definition of the commands in the **application**. 
* Technically it is just a pure DTO. 
* For every **Command** there MUST be at least one **CommandHandler** whose role is to execute that **Command**.
* A **CommandHandler** acts as a port to a Core, therefore it SHOULD NOT handle the domain logic itself, but orchestrate the necessary services instead.
* A **CommandHandler** SHOULD NOT return anything on success, and SHOULD throw a typed Exception on failure. The “no return on success” rule can be broken only when creating entities.
* A CommandHandler MUST implement an interface containing a single public method like this:
```
public void handle(NameOfTheCommand command);
```

#### Command bus (Command Gateway)
It's a mechanism that dispatch the commands to their respective command handlers. Each command is always sent exactly to one command handler,
and if there is not any command handler available for the dispatched command, then an exception will be thrown.

### Query and QueryHandler principles
* Data retrieval SHOULD always go through a **Query**.
* A **Query** describes a **single** data query. It DOES NOT perform it.
* For every **Query** there MUST be at least one **QueryHandler** whose role is to execute that **Query** and return the resulting data set.
* A **QueryHandler** SHOULD return a typed object, and SHOULD throw a typed Exception on failure.
* A **QueryHandler** SHOULD use the existing **ObjectModel** for reads as long as it’s reasonable to do so (in particular for CUD operations in BO migration).
* A **QueryHandler** SHOULD return data objects that make sense to the domain, and SHOULD NOT leak internal objects.
* A **QueryHandler** MUST implement an interface containing a single public method and a typed return like this:
* A **QueryHandler** MUST implement an interface containing a single public method and a typed return like this:
```
public TypeOfReturn handle(NameOfTheQuery query);
```

### Events --> <Noun><PerformedAction>Event
* This is something that has happened in the past. 
* For the business it is the immutable fact that cannot be changed. 
* Keep the definition of the domain event it in the **application**. 
* Technically it's also a DTO object. 

The **AggregateLifecycle.apply()** method from Axon, will publish an event from withing the aggregate and will notify the rest of the application that something was created by publishing the respective event.
This method it will dispatch the event to all event handlers inside the aggregate in order to update the aggregate state.

The methods inside the aggregate that are annotated with **@EventSourcingHandler** annotation, are triggered whenever the event associate with them occurs.

The **@AggregateIdentifier** annotation is used by Axon to associate the respective command with the aggregate.

Both, the **@TargetAggregateIdentifier** **@AggregateIdentifier** annotation help Axon to associate the command dispatched with the right aggregate.

### Message Dispatch Interceptor
It's another way to validate commands before they are processed and relevant events are published.

There are two different types of interceptors: dispatch Interceptors and handler Interceptors. 
1- Dispatch interceptors **are invoked before a message is dispatched to a message handler**. At that point, it may not even be sure that any handler exists for that message. 
2- Handler Interceptors **are invoked just before the message handler is invoked**.

* Message dispatch interceptors are invoked when a command is dispatched on a command bus. 
* They have the ability to alter the command message, by adding metadata, for example, 
  or block the command by throwing an exception. 
* These interceptors are always invoked on the thread that dispatches the command.
Ex.: Let's create a MessageDispatchInterceptor which logs each command message being dispatched on a CommandBus.
```
@Component
@Slf4j
public class CreateProductCommandInterceptor
        implements MessageDispatchInterceptor<CommandMessage<?>> {

    @Override
    public CommandMessage<?> handle(CommandMessage<?> message) {
        return null;
    }

    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(List<? extends CommandMessage<?>> list) {
        return (index, command) -> {
            log.info("Intercepted command: " + command.getPayloadType());
            // checking if the command corresponds to a CreateProductCommand instance
            if (CreateProductCommand.class.equals(command.getPayloadType())) {
                CreateProductCommand createProductCommand =
                        (CreateProductCommand) command.getPayload();

                if (!createProductCommand.getPrice().isPriceGreaterThanZero()) {
                    throw new IllegalArgumentException("Price cannot be les or equal than zero");
                }

                if (!createProductCommand.getTitle().isValidTitle()) {
                    throw new IllegalArgumentException("Title cannot be empty");
                }
            }
            return command;
        };
    }
}
```
We can register this dispatch interceptor with a CommandBus by doing the following:
```
public class CommandBusConfiguration {

    public CommandBus configureCommandBus() {
        CommandBus commandBus = SimpleCommandBus.builder().build();
        commandBus.registerDispatchInterceptor(new MyCommandDispatchInterceptor());
        return commandBus;
    }
}
```

#### Command Handler Interceptors
Message handler interceptors can take action both before and after command processing. Interceptors can even block command processing altogether, for example for security reasons.
Interceptors must implement the MessageHandlerInterceptor interface. This interface declares one method, handle, that takes three parameters: the command message, the current UnitOfWork and an InterceptorChain. The InterceptorChain is used to continue the dispatching process, whereas the UnitOfWork gives you (1) the message being handled and (2) provides the possibility to tie in logic prior, during or after (command) message handling

Unlike dispatch interceptors, handler Interceptors are invoked in the context of the command handler. That means they can attach correlation data based on the message being handled to the unit of work, for example. This correlation data will then be attached to messages being created in the context of that unit of work.
Handler Interceptors are also typically used to manage transactions around the handling of a command. To do so, register a TransactionManagingInterceptor, which in turn is configured with a TransactionManager to start and commit (or roll back) the actual transaction.

Let's create a Message Handler Interceptor which will only allow the handling of commands that contain axonUser as a value for the userId field in the MetaData. If the userId is not present in the meta-data, an exception will be thrown which will prevent the command from being handled. And if the userId's value does not match axonUser, we will also not proceed up the chain.
```
public class MyCommandHandlerInterceptor implements MessageHandlerInterceptor<CommandMessage<?>> {

    @Override
    public Object handle(UnitOfWork<? extends CommandMessage<?>> unitOfWork, InterceptorChain interceptorChain) throws Exception {
        CommandMessage<?> command = unitOfWork.getMessage();
        String userId = Optional.ofNullable(command.getMetaData().get("userId"))
                                .map(uId -> (String) uId)
                                .orElseThrow(IllegalCommandException::new);
        if ("axonUser".equals(userId)) {
            return interceptorChain.proceed();
        }
        return null;
    }
}
```

We can register the handler interceptor with a CommandBus like so:
```
public class CommandBusConfiguration {

    public CommandBus configureCommandBus() {
        CommandBus commandBus = SimpleCommandBus.builder().build();
        commandBus.registerHandlerInterceptor(new MyCommandHandlerInterceptor());
        return commandBus;
    }

}
```

**@CommandHandlerInterceptor Annotation**
The framework has the possibility to add a Handler Interceptor as an @CommandHandlerInterceptor annotated method with on the Aggregate/Entity. The difference between a method on an Aggregate and a "regular" Command Handler Interceptor, is that with the annotation approach you can make decisions based on the current state of the given Aggregate. Some properties of an annotated Command Handler Interceptor are:
* The annotation can be put on entities within the Aggregate. 
* It is possible to intercept a command on Aggregate Root level, whilst the command handler is in a child entity.
* Command execution can be prevented by firing an exception from annotated command handler interceptor.
* It is possible to define an InterceptorChain as a parameter of the command handler interceptor method and use it to control command execution.
* By using the commandNamePattern attribute of the @CommandHandlerInterceptor annotation we can intercept all commands matching provided regular expression.
* Events can be applied from annotated command handler interceptor.

In the listing below we can see a @CommandHandlerInterceptor method which prevents command execution if a command's state field does not match the Aggregate's state field:
```
public class MyAggregate {
    ...
    private String state;
    ...
    @CommandHandlerInterceptor
    public void intercept(MyCommand myCommand, InterceptorChain interceptorChain) {
        if (this.state.equals(myCommand.getState()) {
            interceptorChain.proceed();
        }
    }
}
```

## Docker compose inheritance
To run one of the docker compose files that inherited from the base docker compose file:
```
docker-compose -f docker-compose.dev.yml --env-file ./environment/.env.dev up -d
```

## Spring Cloud – Auto Refresh Config Changes using Spring Cloud Bus
**Spring Cloud Bus** module can be used to link multiple applications with a message broker and we can broadcast configuration changes.
After did all the configurations needed, we can run our microservice and to reload the configuration changes we can trigger POST – http://<hostnema>:<port>/bus/refresh.

Now if you update properties of any microservice and commit the changes, you just need to trigger **/bus/refresh** on any one application. This will automatically broadcast the config changes to all the services that subscribed to RabbitMQ and refresh the properties.

Not only different applications, you may be running multiple instances of same application on different ports. The same process works in these cases also.

So, with Spring Cloud Bus AMQP it is easy to reload configuration changes for any number of applications with one single **/bus/refresh** request.

## Shell script to run the Docker containers
```
./run.sh stop_all <environment>

Ex.:
./run.sh stop_all dev

./run.sh start_all <environment> <build_api>

Ex.: build_api = 0 --> Won't do the Maven compilation. It will be done if the value is equals to 1.
./run.sh start_all dev 0
```

## Define Flyway Migrations
Flyway adheres to the following naming convention for migration scripts:

<Prefix><Version>__<Description>.sql

Where:

* <Prefix> – The default prefix is V, which we can change in the above configuration file using the flyway.sqlMigrationPrefix property.
* <Version> – Migration version number. Major and minor versions may be separated by an underscore. The migration version should always start with 1.
* <Description> – Textual description of the migration. A double underscore separates the description from the version numbers.

Example: 
```
V1_1_0__my_first_migration.sql
```