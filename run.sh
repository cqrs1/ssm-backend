#!/bin/bash

declare project_dir=$(dirname $0)
declare dc_main=${project_dir}/docker-compose.$2.yml
declare env_main=.env.$2
declare has_run_build=$3

function restart() {
    stop_all
    start_all
}

function start() {
    echo "Starting $1..."
    build_api
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} up --build --force-recreate -d $1
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} logs -f
}

function stop() {
    echo "Stopping $1"
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} stop
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} rm -f
}

function start_infra() {
    echo "Starting axonserver, postgres, adminer, rabbitmq, vault, config-server, discovery-service, api-gateway...."
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} up --build --force-recreate -d axonserver products-postgres adminer rabbitmq setup-vault vault config-service api-gateway
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} logs -f
}

function start_all() {
    echo 'Removing dangling images....'
    docker rmi $(docker images --filter dangling=true -q)

    echo "Starting all services...."
    if [ "$has_run_build" -eq 1 ];
    then
      build_api
    fi

    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main}  up --build --force-recreate -d
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} logs -f
}

function stop_all() {
    echo 'Stopping all services....'
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} stop
    docker-compose -f ${dc_main} --env-file=${project_dir}/integration/environment/${env_main} rm -f
    echo 'Removing dangling images....'
    docker rmi $(docker images --filter dangling=true -q)
}

function build_api() {
    mvn clean package -DskipTests
}

action="start_all"

if [[ "$#" != "0"  ]]
then
    action=$@
fi

eval ${action}