--GRANT CONNECT, CREATE ON DATABASE ssm TO ssm_product_service_user;
--CREATE SCHEMA IF NOT EXISTS ssm_products;
--GRANT ALL PRIVILEGES ON SCHEMA ssm_products TO ssm_product_service_user;
--
--DROP TABLE IF EXISTS ssm_products.products;
--CREATE TABLE IF NOT EXISTS ssm_products.products
--  (
--     product_id  VARCHAR(255),
--     title       VARCHAR(255) NOT NULL,
--     price       DECIMAL NOT NULL,
--     quantity    INTEGER NOT NULL,
--     PRIMARY KEY (product_id)
--  );

ALTER TABLE ssm_products.products ALTER COLUMN product_id SET NOT NULL;

COMMIT;