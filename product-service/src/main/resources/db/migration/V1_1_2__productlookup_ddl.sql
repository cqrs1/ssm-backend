GRANT CONNECT, CREATE ON DATABASE ssm TO ssm_product_service_user;
CREATE SCHEMA IF NOT EXISTS ssm_products;
GRANT ALL PRIVILEGES ON SCHEMA ssm_products TO ssm_product_service_user;

CREATE TABLE IF NOT EXISTS ssm_products.productlookup
  (
     product_id  VARCHAR(255) NOT NULL,
     title       VARCHAR(255) NOT NULL,
     PRIMARY KEY (product_id)
  );

ALTER TABLE ssm_products.productlookup ADD CONSTRAINT title_unique UNIQUE (title);

COMMIT;