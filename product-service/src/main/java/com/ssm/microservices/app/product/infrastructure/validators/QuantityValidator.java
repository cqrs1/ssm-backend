package com.ssm.microservices.app.product.infrastructure.validators;

import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.infrastructure.constraints.QuantityConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QuantityValidator implements ConstraintValidator<QuantityConstraint, Quantity> {

    @Override
    public void initialize(QuantityConstraint quantity) {

    }

    @Override
    public boolean isValid(Quantity quantityField,
                           ConstraintValidatorContext constraintValidatorContext) {

        return quantityField != null && quantityField.isAValidQuantity();
    }
}
