package com.ssm.microservices.app.product.application.commands;

import com.ssm.microservices.app.product.domain.base.BaseCommand;
import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import lombok.Builder;
import lombok.Getter;

/**
 * Contains all the information needed to create a new Product.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Getter
public class CreateProductCommand extends BaseCommand<UuId> {
    private final Title title;
    private final Price price;
    private final Quantity quantity;

    @Builder
    public CreateProductCommand(UuId id, Title title, Price price, Quantity quantity) {
        super(id);
        this.title = title;
        this.price = price;
        this.quantity = quantity;
    }
}
