package com.ssm.microservices.app.product.application.commands.handlers;

import com.ssm.microservices.app.product.application.events.ProductCreatedEvent;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

/**
 * This event handler will be used by the Command API
 * to handle the ProductCreatedEvent and persists into
 * the specific lookup table the fields needed.
 * The lookup table will not be used by the Query API.
 * It's part only of the Command API.
 *
 * Processing Group is a logical way to group Event Handlers.
 * it is very much related to the Tracking Event Processor.
 * Each TEP claims its Tracking Token (in order to avoid multiple processing
 * of the same event in different threads/nodes).
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Component
@ProcessingGroup("product-group")
public class ProductLookupEventsHandler {

    @EventHandler
    public void on(ProductCreatedEvent event) {

    }
}
