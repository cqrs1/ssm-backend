package com.ssm.microservices.app.product.application.core;

import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.infrastructure.constraints.PriceConstraint;
import com.ssm.microservices.app.product.infrastructure.constraints.QuantityConstraint;
import com.ssm.microservices.app.product.infrastructure.constraints.TitleConstraint;
import lombok.Data;

@Data
public class CreateProductRestModel {

    @TitleConstraint
    private Title title;

    @PriceConstraint
    private Price price;

    @QuantityConstraint
    private Quantity quantity;

    public CreateProductRestModel() {

    }
}
