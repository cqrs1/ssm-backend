package com.ssm.microservices.app.product.application.query.handlers;

import com.ssm.microservices.app.product.application.events.ProductCreatedEvent;
import com.ssm.microservices.app.product.infrastructure.entity.ProductEntity;
import com.ssm.microservices.app.product.infrastructure.repositories.PostgresDbProductRepository;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The methods in this class will handle the events published
 * by ProductAggregate and they will persist the information
 * about the creation of a Product in the database table.
 *
 * Working with the Axon framework, this kind of classes are
 * called "Projection".
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Component
@ProcessingGroup("product-group")
public class ProductEventsHandler {

    private final PostgresDbProductRepository postgresDbProductRepository;

    @Autowired
    public ProductEventsHandler(PostgresDbProductRepository postgresDbProductRepository) {
        this.postgresDbProductRepository = postgresDbProductRepository;
    }

    @EventHandler
    public void on(ProductCreatedEvent event) {
        ProductEntity productEntity = ProductEntity.builder()
                .productId(event.getId())
                .title(event.getTitle())
                .price(event.getPrice())
                .quantity(event.getQuantity())
                .build();

        postgresDbProductRepository.save(productEntity);
    }
}
