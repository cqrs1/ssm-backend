package com.ssm.microservices.app.product.application.query.handlers;

import com.ssm.microservices.app.product.application.core.ProductRestModel;
import com.ssm.microservices.app.product.application.query.FindProductsQuery;

import java.util.List;

public interface IProductsQueryHandler {

    List<ProductRestModel> handle(FindProductsQuery findProductsQuery);
}
