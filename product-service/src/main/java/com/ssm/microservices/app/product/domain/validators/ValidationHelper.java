package com.ssm.microservices.app.product.domain.validators;


import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

public class ValidationHelper {

    public static final Validator validator = (Validator) Validation.buildDefaultValidatorFactory();

    public static <T> void validate(T object, Class<?>... groups) {
        Set<ConstraintViolation<T>> violations = validator.validate(object, groups);

        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
