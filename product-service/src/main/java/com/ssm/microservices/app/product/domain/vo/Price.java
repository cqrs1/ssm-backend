package com.ssm.microservices.app.product.domain.vo;

import lombok.Value;

import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.function.Function;

@Value
public class Price implements Serializable {
    private static final long serialVersionUID = 5568166042387418524L;

    private BigDecimal value;

    private Price() {
        this.value = null;
    }

    public Price(BigDecimal value) {
//        if (value == null || value.equals(BigDecimal.ZERO)) {
//            throw new IllegalArgumentException("Price cannot be null or lower than 1");
//        }

        this.value = value;
    }

    public Price add(Price otherPrice) {
        return new Price(this.value.add(otherPrice.getValue()));
    }

    public Price subtract(Price otherPrice) {
        return new Price(this.value.subtract(otherPrice.getValue()));
    }

    public String asString() {
        return this.value.toString();
    }

    @Transient
    public boolean isPriceGreaterThanZero() {
        Function<Price, Boolean> greaterThanZeroFn = price ->
                price.getValue().compareTo(BigDecimal.ZERO) > 0;

        Boolean result = greaterThanZeroFn.apply(this);

        return result.booleanValue();
    }

    @Transient
    public boolean isAValidPrice() {
        Function<Price, Boolean> validPriceFn = price ->
                price != null && price.getValue().compareTo(BigDecimal.ZERO) > 0;

        Boolean result = validPriceFn.apply(this);

        return result.booleanValue();
    }
}
