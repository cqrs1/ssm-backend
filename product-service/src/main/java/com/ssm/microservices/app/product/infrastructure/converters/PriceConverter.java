package com.ssm.microservices.app.product.infrastructure.converters;

import com.ssm.microservices.app.product.domain.vo.Price;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.math.BigDecimal;

@Converter(autoApply = true)
public class PriceConverter implements AttributeConverter<Price, BigDecimal> {

    @Override
    public BigDecimal convertToDatabaseColumn(Price attribute) {
        return attribute.getValue();
    }

    @Override
    public Price convertToEntityAttribute(BigDecimal dbData) {
        return new Price(dbData);
    }
}
