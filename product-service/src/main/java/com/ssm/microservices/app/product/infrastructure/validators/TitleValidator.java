package com.ssm.microservices.app.product.infrastructure.validators;

import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.infrastructure.constraints.TitleConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TitleValidator implements ConstraintValidator<TitleConstraint, Title> {

    @Override
    public void initialize(TitleConstraint title) {

    }

    @Override
    public boolean isValid(Title titleField,
                           ConstraintValidatorContext constraintValidatorContext) {

        return titleField != null && titleField.isValidTitle();
    }
}
