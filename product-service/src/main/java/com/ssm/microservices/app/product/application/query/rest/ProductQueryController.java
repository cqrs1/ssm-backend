package com.ssm.microservices.app.product.application.query.rest;

import com.ssm.microservices.app.product.application.core.ProductRestModel;
import com.ssm.microservices.app.product.application.query.FindProductsQuery;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The QueryGateway is used as the mechanism to send query messages.
 * It sends the given query to the QueryBus
 */
@RestController
@RequestMapping("/products")
public class ProductQueryController {

    @Autowired
    QueryGateway queryGateway;

    @GetMapping
    public List<ProductRestModel> getProducts() {
        FindProductsQuery findProductsQuery = new FindProductsQuery();

        List<ProductRestModel> results = queryGateway
                .query(findProductsQuery,
                        ResponseTypes.multipleInstancesOf(ProductRestModel.class))
                .join();

        return results;
    }
}
