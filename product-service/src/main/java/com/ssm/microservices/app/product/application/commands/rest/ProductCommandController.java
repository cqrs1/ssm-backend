package com.ssm.microservices.app.product.application.commands.rest;

import com.ssm.microservices.app.product.application.commands.CreateProductCommand;
import com.ssm.microservices.app.product.application.core.CreateProductRestModel;
import com.ssm.microservices.app.product.domain.vo.UuId;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/products")
public class ProductCommandController {

    private final Environment environment;
    private final CommandGateway commandGateway;

    @Autowired
    public ProductCommandController(Environment environment, CommandGateway commandGateway) {
        this.environment = environment;
        this.commandGateway = commandGateway;
    }

    @PostMapping
    public String createProduct(// @Valid
                                    @RequestBody CreateProductRestModel createProductRestModel) {
        // creating a CreateProductCommand instance
        CreateProductCommand createProductCommand = CreateProductCommand.builder()
                        .price(createProductRestModel.getPrice())
                        .quantity(createProductRestModel.getQuantity())
                        .title(createProductRestModel.getTitle())
                        .id(new UuId(UUID.randomUUID().toString()))
                        .build();

        // sending the command object created to the command bus
        // It will be blocked immediately until the result is available, or
        // until the thread is interrupted
        String returnValue;

        try {
            returnValue = this.commandGateway.sendAndWait(createProductCommand);
        } catch(Exception exception) {
            returnValue = exception.getLocalizedMessage();
        }

        return returnValue;
    }
}
