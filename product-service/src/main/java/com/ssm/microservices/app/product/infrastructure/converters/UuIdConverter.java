package com.ssm.microservices.app.product.infrastructure.converters;

import com.ssm.microservices.app.product.domain.vo.UuId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UuIdConverter implements AttributeConverter<UuId, String> {

    @Override
    public String convertToDatabaseColumn(UuId attribute) {

        return attribute.getValue();
    }

    @Override
    public UuId convertToEntityAttribute(String dbData) {
        return new UuId(dbData);
    }
}
