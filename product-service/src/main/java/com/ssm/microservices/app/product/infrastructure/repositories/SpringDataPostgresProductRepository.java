package com.ssm.microservices.app.product.infrastructure.repositories;

import com.ssm.microservices.app.product.domain.vo.UuId;
import com.ssm.microservices.app.product.infrastructure.entity.ProductEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpringDataPostgresProductRepository extends CrudRepository<ProductEntity, UuId> {
}
