package com.ssm.microservices.app.product.domain.repositories;

import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import com.ssm.microservices.app.product.infrastructure.entity.ProductEntity;

import java.util.List;
import java.util.Optional;

/**
 * To be implemented in the infrastructure layer.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
public interface ProductRepository {

    Optional<ProductEntity> findByProductId(UuId id);
    Optional<ProductEntity> findByProductIdOrTitle(UuId id, Title title);
    void save(ProductEntity productEntity);
    List<ProductEntity> findAll();
}
