package com.ssm.microservices.app.product.domain.vo;

import lombok.Value;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.function.Function;

@Value
public class Quantity implements Serializable {
    private static final long serialVersionUID = -2069886654719975502L;

    private Integer value;

    private Quantity() {
        this.value = null;
    }

    public Quantity(Integer value) {
//        if (value == null || value.intValue() < 1) {
//            throw new IllegalArgumentException("Quantity cannot be null or lower than 1");
//        }
//
//        if (value == null || value.intValue() > 5) {
//            throw new IllegalArgumentException("Quantity cannot be null or larger than 5");
//        }

        this.value = value;
    }

    @Transient
    public boolean isAValidQuantity() {
        Function<Quantity, Boolean> validQuantityFn = quantity ->
                quantity != null && quantity.getValue() >= 1 && quantity.getValue() <= 5;

        Boolean result = validQuantityFn.apply(this);

        return result.booleanValue();
    }

    public Quantity increase(Integer otherQuantity) {
        return new Quantity(this.value + otherQuantity);
    }

    public Quantity decrease(Integer otherQuantity) {
        return new Quantity(this.value - otherQuantity);
    }

    public String asString() {
        return this.value.toString();
    }
}
