package com.ssm.microservices.app.product.infrastructure.entity;

import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import com.ssm.microservices.app.product.infrastructure.converters.PriceConverter;
import com.ssm.microservices.app.product.infrastructure.converters.QuantityConverter;
import com.ssm.microservices.app.product.infrastructure.converters.TitleConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "products", schema = "ssm_products")
public class ProductEntity implements Serializable {
    private static final long serialVersionUID = -1866667812945882107L;

    @EmbeddedId
    @Column(unique = true)
    private UuId productId;

    @Column(unique = true)
    @Convert(converter = TitleConverter.class)
    private Title title;

    @Convert(converter = PriceConverter.class)
    private Price price;

    @Convert(converter = QuantityConverter.class)
    private Quantity quantity;
}
