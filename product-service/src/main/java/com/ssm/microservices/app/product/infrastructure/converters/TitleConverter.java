package com.ssm.microservices.app.product.infrastructure.converters;

import com.ssm.microservices.app.product.domain.vo.Title;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TitleConverter implements AttributeConverter<Title, String> {

    @Override
    public String convertToDatabaseColumn(Title attribute) {
        return attribute.getValue();
    }

    @Override
    public Title convertToEntityAttribute(String dbData) {
        return new Title(dbData);
    }
}
