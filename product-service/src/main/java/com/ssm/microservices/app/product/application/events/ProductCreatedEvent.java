package com.ssm.microservices.app.product.application.events;

import com.ssm.microservices.app.product.domain.base.BaseEvent;
import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import lombok.Builder;
import lombok.Getter;

/**
 * When this event will be published, the new product created
 * will be persisted into the database.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Getter
public class ProductCreatedEvent extends BaseEvent<UuId> {
    private Title title;
    private Price price;
    private Quantity quantity;

    @Builder
    public ProductCreatedEvent(UuId id, Title title, Price price, Quantity quantity) {
        super(id);
        this.title = title;
        this.price = price;
        this.quantity = quantity;
    }

    public ProductCreatedEvent() {
        super();
    }
}
