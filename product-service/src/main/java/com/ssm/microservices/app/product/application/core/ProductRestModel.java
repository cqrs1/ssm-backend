package com.ssm.microservices.app.product.application.core;

import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import lombok.Data;

@Data
public class ProductRestModel {
    private UuId productId;
    private Title title;
    private Price price;
    private Quantity quantity;
}
