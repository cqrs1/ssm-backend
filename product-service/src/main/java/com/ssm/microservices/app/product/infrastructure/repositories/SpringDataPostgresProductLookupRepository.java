package com.ssm.microservices.app.product.infrastructure.repositories;

import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import com.ssm.microservices.app.product.infrastructure.entity.ProductLookupEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpringDataPostgresProductLookupRepository extends CrudRepository<ProductLookupEntity, UuId> {

    Optional<ProductLookupEntity> findByProductIdOrTitle(UuId id, Title title);
}
