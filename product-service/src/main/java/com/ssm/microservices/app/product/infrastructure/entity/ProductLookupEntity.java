package com.ssm.microservices.app.product.infrastructure.entity;

import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import com.ssm.microservices.app.product.infrastructure.converters.TitleConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "productlookup", schema = "ssm_products")
public class ProductLookupEntity implements Serializable {
    private static final long serialVersionUID = 2965489113989797296L;

    @EmbeddedId
    private UuId productId;

    @Column(unique = true)
    @Convert(converter = TitleConverter.class)
    private Title title;
}
