package com.ssm.microservices.app.product.application.commands.interceptors;

import com.ssm.microservices.app.product.application.commands.CreateProductCommand;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.BiFunction;

/**
 * Intercepts the create product command and beisdes do some logging, it
 * will throws an exception if the price or title contained in the command are not valid.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Component
public class CreateProductCommandInterceptor
        implements MessageDispatchInterceptor<CommandMessage<?>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateProductCommandInterceptor.class);
    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(List<? extends CommandMessage<?>> list) {
        return (index, command) -> {
            LOGGER.info("Intercept command: " + command.getPayloadType());

            if (CreateProductCommand.class.equals(command.getPayloadType())) {
                CreateProductCommand createProductCommand = (CreateProductCommand) command.getPayload();

                if (!createProductCommand.getPrice().isPriceGreaterThanZero()) {
                    throw new IllegalArgumentException("Price cannot be les or equal than zero");
                }

                if (!createProductCommand.getTitle().isValidTitle()) {
                    throw new IllegalArgumentException("Title cannot be empty");
                }
            }
            return command;
        };
    }
}
