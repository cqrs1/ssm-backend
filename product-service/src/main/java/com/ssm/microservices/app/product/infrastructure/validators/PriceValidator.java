package com.ssm.microservices.app.product.infrastructure.validators;

import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.infrastructure.constraints.PriceConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PriceValidator implements ConstraintValidator<PriceConstraint, Price> {

    @Override
    public void initialize(PriceConstraint price) {

    }

    @Override
    public boolean isValid(Price priceField,
                           ConstraintValidatorContext constraintValidatorContext) {

        return priceField != null && priceField.isAValidPrice();
    }
}
