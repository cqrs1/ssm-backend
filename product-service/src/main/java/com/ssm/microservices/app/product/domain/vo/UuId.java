package com.ssm.microservices.app.product.domain.vo;

import lombok.Value;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Objects;

@Value
public class UuId implements Serializable {

    private static final long serialVersionUID = 9048570086739060172L;

    @Column(name = "product_id")
    private String value;

    private UuId() {
        this.value = null;
    }

    public UuId(String value) {
        Objects.requireNonNull(value, "Value should not be null");

        if (value.length() == 0) {
            throw new IllegalArgumentException("Id should not be empty");
        }

        this.value = value;
    }
}
