package com.ssm.microservices.app.product.domain.repositories;

import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import com.ssm.microservices.app.product.infrastructure.entity.ProductLookupEntity;

import java.util.List;
import java.util.Optional;

/**
 * To be implemented in the infrastructure layer.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
public interface ProductLookupRepository {

    Optional<ProductLookupEntity> findByProductId(UuId id);
    Optional<ProductLookupEntity> findByProductIdOrTitle(UuId id, Title title);
    void save(ProductLookupEntity productEntity);
    List<ProductLookupEntity> findAll();
}
