package com.ssm.microservices.app.product.infrastructure.constraints;

import com.ssm.microservices.app.product.infrastructure.validators.PriceValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PriceValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PriceConstraint {
    String message() default "Invalid price. It cannot be null or lower than zero";
    Class<?> [] groups() default {};
    Class<? extends Payload> [] payload() default {};
}
