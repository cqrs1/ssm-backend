package com.ssm.microservices.app.product.application.query.handlers;

import com.ssm.microservices.app.product.application.core.ProductRestModel;
import com.ssm.microservices.app.product.application.query.FindProductsQuery;
import com.ssm.microservices.app.product.domain.repositories.ProductRepository;
import com.ssm.microservices.app.product.infrastructure.entity.ProductEntity;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The role of this class is to execute the FindProductsQuery query
 * and return the resulting data set.
 *
 * - A QueryHandler SHOULD return a typed object, and SHOULD throw a
 *   typed Exception on failure.
 * - A QueryHandler MUST implement an interface containing a single public
 *   method and a typed return.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Component
public class ProductsQueryHandler implements IProductsQueryHandler {

    private final ProductRepository productRepository;

    @Autowired
    public ProductsQueryHandler(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @QueryHandler
    @Override
    /**
     * For this method to be able to handle the query, it must be
     * annotated with the @QueryHandler annotation, that will define
     * the method as a "query handler method".
     */
    public List<ProductRestModel> handle(FindProductsQuery findProductsQuery) {
        List<ProductRestModel> products = new ArrayList<>();
        List<ProductEntity> storedProducts = productRepository.findAll();

        products = storedProducts.stream()
                .map(storedProduct -> {
                    ProductRestModel productRestModel = new ProductRestModel();
                    BeanUtils.copyProperties(storedProduct, productRestModel);

                    return productRestModel;
                })
                .collect(Collectors.toList());

        return products;
    }
}
