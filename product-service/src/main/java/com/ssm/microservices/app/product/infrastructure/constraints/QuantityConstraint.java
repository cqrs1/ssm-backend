package com.ssm.microservices.app.product.infrastructure.constraints;

import com.ssm.microservices.app.product.infrastructure.validators.QuantityValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = QuantityValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface QuantityConstraint {
    String message() default "Invalid quantity. It cannot be null or lower than 1 or larger than 5";
    Class<?> [] groups() default {};
    Class<? extends Payload> [] payload() default {};
}
