package com.ssm.microservices.app.product.infrastructure.repositories;

import com.ssm.microservices.app.product.domain.repositories.ProductRepository;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import com.ssm.microservices.app.product.infrastructure.entity.ProductEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class PostgresDbProductRepository implements ProductRepository {

    private final SpringDataPostgresProductRepository productRepository;

    @Autowired
    public PostgresDbProductRepository(SpringDataPostgresProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Optional<ProductEntity> findByProductId(UuId id) {
        return Optional.empty();
    }

    @Override
    public Optional<ProductEntity> findByProductIdOrTitle(UuId id, Title title) {
        return Optional.empty();
    }

    @Override
    public void save(ProductEntity productEntity) {
        this.productRepository.save(productEntity);
    }

    @Override
    public List<ProductEntity> findAll() {
        List<ProductEntity> productsList = new ArrayList<>();
        this.productRepository.findAll().forEach(productsList::add);

        return productsList;
    }
}
