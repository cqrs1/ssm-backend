package com.ssm.microservices.app.product.domain.aggregate;

import com.ssm.microservices.app.product.application.commands.CreateProductCommand;
import com.ssm.microservices.app.product.application.events.ProductCreatedEvent;
import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.lang.reflect.InvocationTargetException;

/**
 * With the @Aggregate annotation, Axon will know that this
 * is not just a regular class, but an aggregate too.
 *
 * This aggregate will have two constructors:
 * - The first one it will be a no args constructor that is required by the Axon framework
 *   to create a new instance of this class.
 * - The second one it will be used as a Command Handler method, and it will be invoked when
 *   the createProductCommand is dispatched.
 *   It will be annotated with the @CommandHandler annotation
 *
 * The @AggregateIdentifier annotation is used by Axon to associate the respective
 * command with the aggregate.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@Aggregate
public class ProductAggregate {

    @AggregateIdentifier
    private UuId productId;
    private Title title;
    private Price price;
    private Quantity quantity;

    public ProductAggregate() {

    }

    /**
     * Used to create a new instance of the aggregate and will validate
     * the createProductCommand.
     *
     * @param createProductCommand
     */
    @CommandHandler
    public ProductAggregate(CreateProductCommand createProductCommand) throws InvocationTargetException, IllegalAccessException {

        // validate CreateProductCommand
        if (!createProductCommand.getPrice().isAValidPrice()) {
            throw new IllegalArgumentException("Price cannot be les or equal than zero");
        }

        if (!createProductCommand.getTitle().isValidTitle()) {
            throw new IllegalArgumentException("Title cannot be empty");
        }

        // creating a ProductCreatedEvent instance and publish it
//        BeanUtils.copyProperties(createProductCommand, productCreatedEvent);
        ProductCreatedEvent productCreatedEvent = ProductCreatedEvent.builder()
                .title(createProductCommand.getTitle())
                .price(createProductCommand.getPrice())
                .quantity(createProductCommand.getQuantity())
                .id(createProductCommand.getId())
                .build();

        AggregateLifecycle.apply(productCreatedEvent);
    }

    /**
     * This event handler method (event sourcing handler method) it will be used to update the
     * aggregate state with new information.
     * It will initialize the current state of the aggregate with the latest information.
     * @param productCreatedEvent
     */
    @EventSourcingHandler
    public void on(ProductCreatedEvent productCreatedEvent) {
        this.productId = productCreatedEvent.getId();
        this.price = productCreatedEvent.getPrice();
        this.quantity = productCreatedEvent.getQuantity();
        this.title = productCreatedEvent.getTitle();
    }
}
