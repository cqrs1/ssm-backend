package com.ssm.microservices.app.product.domain.vo;

import lombok.Value;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.function.Function;

@Value
public class Title  implements Serializable {

    private static final long serialVersionUID = -6590951147842303246L;

    private String value;

    private Title() {
        this.value = null;
    }

    public Title(String value) {
//        if (value == null || value.trim().isEmpty()) {
//            throw new IllegalArgumentException("Title should not be empty");
//        }

        this.value = value;
    }

    public Title change(String value) {
        return new Title(value);
    }

    @Transient
    public boolean isValidTitle() {
        Function<Title, Boolean> isValidTitleFn = title ->
                title.getValue() != null && !title.getValue().trim().isEmpty();

        Boolean result = isValidTitleFn.apply(this);
        return result.booleanValue();
    }
}
