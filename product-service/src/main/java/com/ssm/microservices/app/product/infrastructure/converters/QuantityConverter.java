package com.ssm.microservices.app.product.infrastructure.converters;

import com.ssm.microservices.app.product.domain.vo.Quantity;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class QuantityConverter implements AttributeConverter<Quantity, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Quantity attribute) {
        return attribute.getValue();
    }

    @Override
    public Quantity convertToEntityAttribute(Integer dbData) {
        return new Quantity(dbData);
    }
}
