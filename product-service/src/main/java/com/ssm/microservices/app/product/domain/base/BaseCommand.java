package com.ssm.microservices.app.product.domain.base;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

/**
 * The @TargetAggregateIdentifier annotation is an Axon specific requirement
 * to identify the aggregate instance.
 *
 * This annotation is required for Axon to determine the instance of the Aggregate
 * that should handle the command.
 *
 * @param <T>
 * @author Ernesto A. Rodriguez Acosta
 */
@Getter
@AllArgsConstructor
public class BaseCommand<T> {

    @TargetAggregateIdentifier
    public T id;
}
