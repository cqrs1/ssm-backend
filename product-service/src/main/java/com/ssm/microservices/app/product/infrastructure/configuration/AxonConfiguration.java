package com.ssm.microservices.app.product.infrastructure.configuration;

import com.ssm.microservices.app.product.application.commands.interceptors.CreateProductCommandInterceptor;
import org.axonframework.commandhandling.CommandBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AxonConfiguration {

    @Autowired
    public void registerInterceptors(CommandBus commandBus) {
        commandBus.registerDispatchInterceptor(new CreateProductCommandInterceptor());
    }
}
