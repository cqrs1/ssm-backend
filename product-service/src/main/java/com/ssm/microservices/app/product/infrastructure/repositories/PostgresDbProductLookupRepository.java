package com.ssm.microservices.app.product.infrastructure.repositories;

import com.ssm.microservices.app.product.domain.repositories.ProductLookupRepository;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import com.ssm.microservices.app.product.infrastructure.entity.ProductLookupEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class PostgresDbProductLookupRepository implements ProductLookupRepository {

    private final SpringDataPostgresProductLookupRepository productLookupRepository;

    @Autowired
    public PostgresDbProductLookupRepository(SpringDataPostgresProductLookupRepository productLookupRepository) {
        this.productLookupRepository = productLookupRepository;
    }

    @Override
    public Optional<ProductLookupEntity> findByProductId(UuId id) {
        return this.productLookupRepository.findById(id);
    }

    @Override
    public Optional<ProductLookupEntity> findByProductIdOrTitle(UuId id, Title title) {
        return this.productLookupRepository.findByProductIdOrTitle(id, title);
    }

    @Override
    public void save(ProductLookupEntity productLookupEntity) {
        this.productLookupRepository.save(productLookupEntity);
    }

    @Override
    public List<ProductLookupEntity> findAll() {
        List<ProductLookupEntity> productsLookupList = new ArrayList<>();
        this.productLookupRepository.findAll().forEach(productsLookupList::add);

        return productsLookupList;
    }
}
