package com.ssm.microservices.app.product;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java8Streams {

    private static final Employee [] arrayOfEmps = {
            new Employee(1, "Jeff Bezoz",
                    new BigDecimal(100000).setScale(2, RoundingMode.HALF_UP).doubleValue()),
            new Employee(2, "Bill Gates",
                    new BigDecimal(200000).setScale(2, RoundingMode.HALF_UP).doubleValue()),
            new Employee(3, "Mark Zuckerberg",
                    new BigDecimal(300000).setScale(2, RoundingMode.HALF_UP).doubleValue())
    };

    private static final List<Employee> empList = Arrays.asList(arrayOfEmps);

    private static final Integer [] empIds = { 1, 2, 3 };

    /**
     * forEach() loops over the stream elements, calling the supplied function
     * on each element.
     * It's a terminal operation.
     */
    @Test
    public static void whenIncrementSalaryForEachEmployee_thenApplyNewSalary() {
        empList.stream()
        .forEach(e -> e.salaryIncrement(10.0));

        Assertions.assertThat(empList)
                .extracting("salary")
                .contains(100010.0, 200010.0, 300010.0);
    }

    /**
     * collect() performs mutable fold operations (repackaging elements
     * to some data structures and applying some additional logic, concatenating them,
     * ecc) on data elements held in the Stream instance.
     */
    @Test
    public static void whenCollectStreamToList_thenGetList() {
        List<Employee> employees = empList.stream().collect(Collectors.toList());
        Assert.assertEquals(empList, employees);
    }

    /**
     * filter() produces a new stream that contains elements of the
     * original stream that pass a given test (specified by a Predicate).
     */
    @Test
    public static void whenFilterEmèployees_thenGetFilteredStream() {
        // filtering out null references for employees and then
        // applying filter to only keep employees with salaries over a certain threshold
        List<Employee> employees = empList.stream()
                .filter(e -> e != null)
                .filter(e -> e.getSalary() > 200000)
                .collect(Collectors.toList());

        Assert.assertEquals(1, employees.size());
    }

    /**
     * findFirst() returns an Optional for the first entry in the
     * stream; the Optional can, of course, be empty.
     */
    @Test
    public static void whenFindFirst_thenGetFirstEmployeeInStream() {
        // returns the first employee with the salary greater than 100000
        Employee employee = empList.stream()
                .filter(e -> e != null)
                .filter(e -> e.getSalary() > 100000)
                .findFirst()
                .orElse(null);

        Assert.assertEquals(employee.getSalary(), new Double(200000));
    }

    /**
     * toArray() allows to get an array out of the stream.
     * The syntax Employee[]::new creates an empty array of Employee,
     * which is then filled with elements from the stream.
     */
    @Test
    public void whenStreamToArray_thenGetArray() {
        Employee [] employees = empList.stream()
                .toArray(Employee[]::new);

        Assert.assertEquals(empList.toArray(), employees);
    }

    /**
     * A stream can hold complex data structures like Stream<List<String>>.
     * In cases like this, flatMap() help us to flatten the data structure
     * to simplify further operations.
     * Here, with it, we convert the Stream<List<String>> to a simpler Stream<String>
     */
    @Test
    public void whenFlatMapEmployeeNames_thenGetNameStream() {
        List<List<String>> namesNested = Arrays.asList(
                Arrays.asList("Jeff", "Bezos"),
                Arrays.asList("Bill", "Gates"),
                Arrays.asList("Marck", "Zuckerberg")
        );

        List<String> namesFlatStream = namesNested.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        Assert.assertEquals(namesFlatStream.size(), namesNested.size() * 2);
    }

    /**
     * peek allows to perform multiple operations on each element
     * of the stream before any terminal operation is applied.
     *
     * It performs the specified operation on each element of the stream
     * and returns a new stream which can be used further.
     *
     * peek() is an intermediate operation.
     */
    @Test
    public void whenIncrementSalaryUsingPeek_thenApplyNewSalary() {

        List<Employee> empList = Arrays.asList(arrayOfEmps);

        empList.stream()
                .peek(e -> e.salaryIncrement(10.0))
                .peek(System.out::println)
                .collect(Collectors.toList());

        Assertions.assertThat(empList)
                .extracting("salary")
                .contains(100010.0, 200010.0, 300010.0);
    }

    /**
     * A sample stream pipeline, where empList is the source,
     * filter() is the intermediate operation and count() is
     * the terminal operation.
     */
    @Test
    public void whenStreamCount_thenGetElementCount() {
        Long empCount = empList.stream()
                .filter(e -> e.getSalary() > 200000)
                .count();

        Assert.assertEquals(empCount, new Long(1));
    }

    /**
     * Some operations are deemed "short-circuit operations"
     * because they allow computations on infinite streams to
     * complete in finite time.
     *
     * Here, we use short-circuit operations skip() to skip first
     * 3 elements, and limit() to limit to 5 elements from the
     * infinite stream generated using iterate().
     */
    @Test
    public void whenLimitInfiniteStream_thenGetFiniteElements() {
        Stream<Integer> infiniteStream = Stream.iterate(2, i -> i * 2);

        List<Integer> collect = infiniteStream
                .skip(3)
                .limit(5)
                .peek(System.out::println)
                .collect(Collectors.toList());

        Assert.assertEquals(collect, Arrays.asList(16, 32, 64, 128, 256));
    }
}
