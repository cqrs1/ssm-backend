package com.ssm.microservices.app.product.domain.aggregate;

import com.ssm.microservices.app.product.domain.vo.Title;

import java.util.function.Function;

public class LambdaTesting {

    public static void main(String[] args) {
        Title title = new Title("");

        Function<Title, Boolean> isValidTitleFn = t -> {
            System.out.println("t.getValue() != null: " + t.getValue() != null);
            System.out.println("!t.getValue().trim().isEmpty(): " + !t.getValue().trim().isEmpty());
            Boolean bol = t.getValue() != null && !t.getValue().trim().isEmpty();
            return bol;
        };

        Boolean result = isValidTitleFn.apply(title);
        System.out.println("Valid title: " + result);
    }
}
