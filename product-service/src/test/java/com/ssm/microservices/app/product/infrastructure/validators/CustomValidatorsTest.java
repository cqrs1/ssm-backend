package com.ssm.microservices.app.product.infrastructure.validators;

import com.ssm.microservices.app.product.BaseTest;
import com.ssm.microservices.app.product.application.commands.rest.ProductCommandController;
import com.ssm.microservices.app.product.application.core.CreateProductRestModel;
import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.domain.vo.Title;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CustomValidatorsTest extends BaseTest {

    private MockMvc mockMvc;

    private CreateProductRestModel createProductRestModel;

    @Mock
    private Environment environment;

    @Mock
    private CommandGateway commandGateway;

    @InjectMocks
    private final ProductCommandController productCommandController =
            new ProductCommandController(environment, commandGateway);

    @BeforeEach
    public void setUp() {
        createProductRestModel = new CreateProductRestModel();
        createProductRestModel.setPrice(new Price(BigDecimal.valueOf(4)));
        createProductRestModel.setTitle(new Title("Test"));
        createProductRestModel.setQuantity(new Quantity(2));

        this.mockMvc = MockMvcBuilders.standaloneSetup(productCommandController)
                .build();
    }

    @Test
    public void givenCreateProductUriAndWrongTitleInModel_thenExpectBadRequestResponse() throws Exception {

        createProductRestModel.setTitle(new Title(""));

        this.mockMvc.perform(MockMvcRequestBuilders.post("/products")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(asJsonString(createProductRestModel)))
        .andExpect(status().isBadRequest());
    }

    @Test
    public void givenCreateProductUriAndRightModel_thenReturnOKResponse() throws Exception {
        CreateProductRestModel createProductRestModel = new CreateProductRestModel();
        createProductRestModel.setPrice(new Price(BigDecimal.valueOf(20)));
        createProductRestModel.setTitle(new Title("Test"));
        createProductRestModel.setQuantity(new Quantity(2));

        this.mockMvc.perform(MockMvcRequestBuilders.post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(createProductRestModel)))
                .andExpect(status().isOk());
    }

    @Test
    public void givenCreateProductUriAndWrongQuantityInModel_thenExpectBadRequestResponse() throws Exception {

        createProductRestModel.setQuantity(new Quantity(-1));

        this.mockMvc.perform(MockMvcRequestBuilders.post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(createProductRestModel)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void givenCreateProductUriAndWrongPriceInModel_thenExpectBadRequestResponse() throws Exception {

        createProductRestModel.setPrice(new Price(BigDecimal.valueOf(0)));

        this.mockMvc.perform(MockMvcRequestBuilders.post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(createProductRestModel)))
                .andExpect(status().isBadRequest());
    }
}
