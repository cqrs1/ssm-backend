package com.ssm.microservices.app.product;

import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

public class BaseTest {

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
