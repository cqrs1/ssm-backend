package com.ssm.microservices.app.product.domain.aggregate;

import com.ssm.microservices.app.product.application.commands.CreateProductCommand;
import com.ssm.microservices.app.product.application.events.ProductCreatedEvent;
import com.ssm.microservices.app.product.domain.vo.Price;
import com.ssm.microservices.app.product.domain.vo.Quantity;
import com.ssm.microservices.app.product.domain.vo.Title;
import com.ssm.microservices.app.product.domain.vo.UuId;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.UUID;

public class ProductAggregateUnitTest {

    private static final UuId PRODUCT_ID = new UuId(UUID.randomUUID().toString());

    private FixtureConfiguration<ProductAggregate> fixture;

    @BeforeEach
    void setUp() {
        fixture = new AggregateTestFixture<>(ProductAggregate.class);
    }

    @Test
    void whenCreateProductCommand_thenShouldPublishProductCreatedEvent() {
        CreateProductCommand createProductCommand = CreateProductCommand.builder()
                .price(new Price(BigDecimal.valueOf(200)))
                .quantity(new Quantity(10))
                .title(new Title("Coco"))
                .id(PRODUCT_ID)
                .build();

        ProductCreatedEvent productCreatedEvent = ProductCreatedEvent.builder()
                .title(createProductCommand.getTitle())
                .price(createProductCommand.getPrice())
                .quantity(createProductCommand.getQuantity())
                .id(createProductCommand.getId())
                .build();

        fixture.givenNoPriorActivity()
                .when(createProductCommand)
                .expectEvents(productCreatedEvent);
    }
}
