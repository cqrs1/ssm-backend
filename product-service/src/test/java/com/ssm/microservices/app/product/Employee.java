package com.ssm.microservices.app.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private Integer id;
    private String completeName;
    private Double salary;

    public void salaryIncrement(Double increment) {
        this.salary += increment;
    }
}
